#!/usr/bin/env bash
MONERO="$(which monerod)"

# Network switch
if [ "${MONERO_NETWORK}" = "mainnet" ]; then
  FLAGS=''
elif [ "${MONERO_NETWORK}" = "testnet" ]; then
  FLAGS='--testnet'
elif [ "${MONERO_NETWORK}" = "regtest" ]; then
  FLAGS='--regtest'
fi

${MONERO} --version

echo "start monero daemon with flags \"${FLAGS}\""
echo "rpc-login ${MONERO_USER}:${MONERO_PASSWORD}"

# shellcheck disable=SC1009
# shellcheck disable=SC1073
# shellcheck disable=SC1072
${MONERO} "${FLAGS}" \
          --data-dir "${MONERO_HOME}" \
          --p2p-bind-ip="${MONERO_HOST}" \
          --p2p-bind-port="${MONERO_P2P_PORT}" \
          --rpc-bind-ip="${MONERO_HOST}" \
          --rpc-bind-port="${MONERO_RCP_PORT}" \
          --non-interactive \
          --confirm-external-bind \
          --rpc-login "${MONERO_USER}:${MONERO_PASSWORD}" \