FROM debian:bookworm

ENV MONERO_USER monero
ENV MONERO_PASSWORD passw0rd
ENV MONERO_HOME /home/$MONERO_USER
ENV MONERO_NETWORK mainnet
ENV MONERO_HOST "0.0.0.0"

RUN mkdir -p /wallet \
             /data \
             ${MONERO_HOME} && \
    ln -sf /data ${MONERO_HOME}/.bitmonero

COPY src/entrypoint.bash /usr/local/bin/

RUN chmod +x /usr/local/bin/entrypoint.bash && \
    adduser --system --group --disabled-password ${MONERO_USER} --home ${MONERO_HOME} && \
	  chown -R ${MONERO_USER} ${MONERO_HOME} \
             /data \
             /wallet && \
    apt -y update && \
    apt -y install \
           monero \
           jq

WORKDIR $MONERO_HOME
VOLUME /data
VOLUME /wallet

EXPOSE 18080
EXPOSE 18081

USER monero

ENTRYPOINT ["entrypoint.bash"]